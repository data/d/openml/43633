# OpenML dataset: NYC-Housing-Data-2003-2019

https://www.openml.org/d/43633

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This dataset is created for the prediction of future New York Housing Price based on the past 17 years of record. 
Content
Please check the details under the column description.
Acknowledgements
New York City Department of Finance Open Source Data.
If there is any violation, I am willing to delete the dataset.
Inspiration
DDL of Schol Project

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43633) of an [OpenML dataset](https://www.openml.org/d/43633). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43633/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43633/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43633/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

